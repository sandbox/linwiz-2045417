CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Disable Delete Attachments relies on the upload module from Drupal core.
It allows you to grant or deny users the permission to remove uploaded
files even if they have permission to upload. Set the permissions and
you are done.


INSTALLATION
------------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

After installation you can configure permissions per role either allowing
or denying users the ability to delete uploads.